package br.com.projuris;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MyCalculo implements Calculo {

	@Override
	public List<CustoCargo> custoPorCargo(List<Funcionario> funcionarios) {
		if (funcionarios == null) {
			return Collections.emptyList();
		}
		final List<CustoCargo> resultado = new ArrayList<CustoCargo>();

		for (Funcionario funcionario : funcionarios) {
			CustoCargo custoCargo = getOrAddByCargo(funcionario.getCargo(), resultado);
			BigDecimal currentCusto = custoCargo.getCusto();
			BigDecimal newCusto = currentCusto.add(funcionario.getSalario());
			custoCargo.setCusto(newCusto);
		}
		return resultado;
	}

	private CustoCargo getOrAddByCargo(final String cargo, final List<CustoCargo> resultado) {
		CustoCargo byCargo = getByCargo(cargo, resultado);
		if (byCargo != null) {
			return byCargo;
		}
		CustoCargo custoCargo = new CustoCargo();
		custoCargo.setCargo(cargo);
		custoCargo.setCusto(BigDecimal.ZERO);
		resultado.add(custoCargo);
		return custoCargo;
	}

	private CustoCargo getByCargo(final String cargo, final List<CustoCargo> resultado) {
		for (CustoCargo custoCargo : resultado) {
			if (cargo.equals(custoCargo.getCargo())) {
				return custoCargo;
			}
		}
		return null;
	}

	@Override
	public List<CustoDepartamento> custoPorDepartamento(List<Funcionario> funcionarios) {
		if (funcionarios == null) {
			return Collections.emptyList();
		}
		final List<CustoDepartamento> resultado = new ArrayList<CustoDepartamento>();

		for (Funcionario funcionario : funcionarios) {
			CustoDepartamento custoDepartamento = getOrAddByDepartamento(funcionario.getDepartamento(), resultado);
			BigDecimal currentCusto = custoDepartamento.getCusto();
			BigDecimal newCusto = currentCusto.add(funcionario.getSalario());
			custoDepartamento.setCusto(newCusto);
		}
		return resultado;
	}

	private CustoDepartamento getOrAddByDepartamento(final String cargo, final List<CustoDepartamento> resultado) {
		CustoDepartamento byDepartamento = getByDepartamento(cargo, resultado);
		if (byDepartamento != null) {
			return byDepartamento;
		}
		CustoDepartamento custoDepartamento = new CustoDepartamento();
		custoDepartamento.setDepartamento(cargo);
		custoDepartamento.setCusto(BigDecimal.ZERO);
		resultado.add(custoDepartamento);
		return custoDepartamento;
	}

	private CustoDepartamento getByDepartamento(final String cargo, final List<CustoDepartamento> resultado) {
		for (CustoDepartamento custoDepartamento : resultado) {
			if (cargo.equals(custoDepartamento.getDepartamento())) {
				return custoDepartamento;
			}
		}
		return null;
	}
}
