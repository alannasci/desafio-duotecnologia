$(document).ready(function () {
    /**
     * @return {string}
     */
    var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
            onKeyPress: function (val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };
    $('.tel-input').mask(SPMaskBehavior, spOptions);
    $('.cpf-input').mask('000.000.000-00');
    $('.rg-input').mask('00.000.000-A');
    $('.date-input').mask('00/00/0000');

});